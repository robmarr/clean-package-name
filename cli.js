import {readFile as _readFile} from 'fs'
import {join} from 'path'
import cleanPackageName from '.'

const readFile = file => new Promise((resolve, reject) => (
  _readFile(file, 'utf8', (err, data) => (err ? reject(err) : resolve(data)))
))

const run = async () => {
  const hasNoScopeArg = process.argv.includes('--no-scope') || process.argv.includes('-n')
  let {npm_package_name: name} = process.env
  if (!name) {
    try {
      const pkgFile = await readFile(join(process.cwd(), 'package.json'))
      name = JSON.parse(pkgFile).name
      if (!name) {
        throw new Error('no name')
      }
    } catch (error) {
      throw new Error('Could not find package name!')
    }
  }

  const clean = cleanPackageName(name, {scope: !hasNoScopeArg})
  process.stdout.write(clean)
  return clean
}

/* istanbul ignore if */
if (!module.parent) {
  run()
}

export default run
