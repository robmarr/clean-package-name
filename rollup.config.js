import {builtinModules as external} from 'module'
import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import minify from 'rollup-plugin-babel-minify'
import shebang from '@robmarr/rollup-plugin-shebang'
// It breaks if we don't supply the json extension
// eslint-disable-next-line import/extensions
import {name} from './package.json'

const plugins = [resolve({preferBuiltins: true}), commonjs(), minify({comments: false})]

export default [{
  input: 'cli.js',
  external,
  plugins: [...plugins, shebang()],
  output: [{
    name,
    file: 'dist/cli.js',
    format: 'cjs'
  }]
}]

