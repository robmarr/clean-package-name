export const stripInvalidChars = name => name.replace(/[^\w-/]/g, '')

export const removeScope = name => name.replace(/^@[^/]+\//, '')

export default function (name, opts = {}) {
  const {
    scope = true
  } = opts
  return stripInvalidChars(scope ? name : removeScope(name)).toLowerCase()
}
