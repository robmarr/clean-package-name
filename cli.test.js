import {join} from 'path'
import test from 'ava'
import {default as run} from './cli'

const terriblePackageName = '@hello|-wor£ld/"Fo§o_b{A}r"'

const envVarName = 'npm_package_name'
const fixturesDir = join(__dirname, 'test', 'fixtures')

const envCache = JSON.stringify(process.env)
const argvCache = JSON.stringify(process.argv)

test.beforeEach(() => {
  process.argv = []
  process.env = {}
})

test.after.always(() => {
  process.argv = JSON.parse(argvCache)
  process.env = JSON.parse(envCache)
})

test('uses package name from npm_package_name environment variable', async t => {
  t.plan(1)
  process.env[envVarName] = terriblePackageName
  const result = await run()
  t.is(result, 'hello-world/foo_bar')
})

test('reads package name from package.json if available', async t => {
  t.plan(1)
  delete process.env[envVarName]
  process.chdir(join(fixturesDir, 'package'))
  const result = await run()
  t.is(result, 'hello-world/foobar')
})

test('throws if no package.json found', t => {
  t.plan(1)
  process.chdir(join(fixturesDir, 'no-package'))
  return t.throwsAsync(run)
})

test('throws if no name field in package.json', t => {
  t.plan(1)
  process.chdir(join(fixturesDir, 'no-name'))
  return t.throwsAsync(run)
})

test('throws if package.json is invalid json', t => {
  t.plan(1)
  process.chdir(join(fixturesDir, 'invalid-json'))
  return t.throwsAsync(run)
})

test('--no-scope or -n option removes scope', async t => {
  t.plan(3)
  process.env[envVarName] = '@hello/world'
  t.is(await run(), 'hello/world')
  process.argv = ['-n']
  t.is(await run(), 'world')
  process.argv = ['--no-scope']
  t.is(await run(), 'world')
})
