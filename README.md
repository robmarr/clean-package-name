# clean-package-name

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com) [![Coverage Status](https://coveralls.io/repos/gitlab/robmarr/clean-package-name/badge.svg?branch=main)](https://coveralls.io/gitlab/robmarr/clean-package-name?branch=main) [![Pipeline Status](https://gitlab.com/robmarr/clean-package-name/badges/main/pipeline.svg)](https://gitlab.com/robmarr/clean-package-name/-/commits/main) [![npm version](https://img.shields.io/npm/v/@robmarr/clean-package-name/latest.svg)](https://www.npmjs.com/package/@robmarr/clean-package-name) [![chat](https://img.shields.io/discord/536945285989924864.svg)](https://discord.gg/UgcRF6t)

A script that cleans up a package name for use in other scripts.

## Usage

This script will attempt to read the package name from the environment via `npm_package_name` or by reading the local package.json.

It will then either trim problem characters from the name: 

```javascript
npx @robmarr/clean-package-name
// returns scope/clean-package-name
// (anything but letters, numbers, underscores, dashes and forward slashes are removed)
```

The npm scope of a package can be removed via the `--no-scope` option.

```javascript
npx @robmarr/clean-package-name --no-scope
// returns clean-package-name
// (again removes problem characters but then returns everything after the first forward slash)
```

## License

Unless stated otherwise all works are:

- Copyright &copy; Robin Marr

and licensed under:

- [ISC License](https://opensource.org/licenses/ISC)
