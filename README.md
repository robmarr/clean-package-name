# clean-package-name

[![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo) [![Coverage Status](https://coveralls.io/repos/bitbucket/robmarr/clean-package-name/badge.svg?branch=master)](https://coveralls.io/bitbucket/robmarr/clean-package-name?branch=master) [![Build Status](https://img.shields.io/bitbucket/pipelines/robmarr/clean-package-name.svg)](https://bitbucket.org/robmarr/clean-package-name/addon/pipelines/home) [![npm version](https://img.shields.io/npm/v/@robmarr/clean-package-name/latest.svg)](https://www.npmjs.com/package/@robmarr/clean-package-name) [![chat](https://img.shields.io/discord/536945285989924864.svg)](https://discord.gg/UgcRF6t)

A script that cleans up a package name for use in other scripts.

## Usage

This script will attempt to read the package name from the environment via `npm_package_name` or by reading the local package.json.

It will then either trim problem characters from the name: 

```javascript
npx @robmarr/clean-package-name
// returns scope/clean-package-name
// (anything but letters, numbers, underscores, dashes and forward slashes are removed)
```

Most lit html directives are supported via [@popeindustries/lit-html-server](https://github.com/popeindustries/lit-html-server).

```javascript
npx @robmarr/clean-package-name --no-scope
// returns clean-package-name
// (again removes problem characters but then returns everything after the first forward slash)
```

## License

Unless stated otherwise all works are:

- Copyright &copy; Robin Marr

and licensed under:

- [ISC License](https://opensource.org/licenses/ISC)
