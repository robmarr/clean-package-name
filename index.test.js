import test from 'ava'
import {default as cleanPackageName, removeScope, stripInvalidChars} from '.'

const terriblePackageName = '@hello|-wor£ld/"Fo§o_b{A}r"'

test('strips invalid characters (everything that isn\'t a letter, number, underscore (_), dash (-) or forward slash (/) )', t => {
  t.plan(1)
  t.is(stripInvalidChars(terriblePackageName), 'hello-world/Foo_bAr')
})

test('remove everything up to the first forward slash (/) if the string starts with the @ symbol', t => {
  t.plan(1)
  t.is(removeScope(terriblePackageName), '"Fo§o_b{A}r"')
})

test('removes invalid chars and converts to lowercase', t => {
  t.plan(1)
  t.is(cleanPackageName(terriblePackageName), 'hello-world/foo_bar')
})

test('optionally removes scope', t => {
  t.plan(1)
  t.is(cleanPackageName(terriblePackageName, {scope: false}), 'foo_bar')
})
